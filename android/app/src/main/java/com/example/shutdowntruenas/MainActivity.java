package com.example.shutdowntruenas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.net.URI;
import java.net.URISyntaxException;
//import javax.websocket.*;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        EditText ip = findViewById(R.id.ip);
        EditText user = findViewById(R.id.user);
        EditText pw = findViewById(R.id.pw);
        WSClient client = null;
        try {
            client = new WSClient(
                    new URI(String.format("ws://%s/websocket", ip.getText().toString())),
                    user.getText().toString(),
                    pw.getText().toString()
            );
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        client.connect();
    }
}
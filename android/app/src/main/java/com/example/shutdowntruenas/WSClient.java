package com.example.shutdowntruenas;

import android.util.Log;

import java.net.URI;
import java.nio.ByteBuffer;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

public class WSClient extends WebSocketClient {

    final String TAG = "WSClient";

    String user;
    String pw;
    String[] msgs;
    int i;

    public WSClient(URI serverURI, String user, String pw) {
        super(serverURI);
        this.user = user;
        this.pw = pw;
        this.msgs = new String[3];
        this.i = 0;
    }

    private void buildMsgs() {
        this.msgs[0] = "{\"msg\":\"connect\",\"version\":\"1\",\"support\":[\"1\"]}";
        this.msgs[1] = String.format(
                "{\"id\":\"0\",\"msg\":\"method\",\"method\":\"auth.login\",\"params\":[\"%s\",\"%s\"]}",
                this.user, this.pw
        );
        this.msgs[2] = "{\"id\":\"0\",\"msg\":\"method\",\"method\":\"system.shutdown\",\"params\":{}}";
    }

    private void sendNext() {
        if (this.i == this.msgs.length){
            return;
        }
        this.send(this.msgs[this.i++]);
        Log.d(this.TAG, "sent message: " + this.msgs[this.i-1]);
    }

    @Override
    public void onOpen(ServerHandshake handshakeData) {
        this.buildMsgs();
        Log.d(this.TAG, "open connection");
        this.sendNext();
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        Log.d(this.TAG, "closed with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(String message) {
        Log.d(this.TAG, "received message: " + message);
        this.sendNext();
    }

    @Override
    public void onMessage(ByteBuffer message) {
        Log.d(this.TAG, "received ByteBuffer");
        this.sendNext();
    }

    @Override
    public void onError(Exception ex) {
        Log.e(this.TAG, "an error occurred:", ex);
    }
}

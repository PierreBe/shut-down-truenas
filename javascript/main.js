"use strict";

let msgs = [{
    msg: 'connect',
    version: '1',
    support: ['1']
}, {
    id: '0',
    msg: 'method',
    method: 'auth.login',
    // params: [user.value, pw.value]
}, {
    id: '0',
    msg: 'method',
    method: 'system.shutdown',
    params: {}
}];
let i = 0;
function sendNextMsg(webSocket) {
    // console.dir(msgs[i]);
    webSocket.send(JSON.stringify(msgs[i++]));
}

main.addEventListener('submit', (event) => {
    event.preventDefault();
    let url = `ws://${ip.value}/websocket`;
    msgs[1].params = [user.value, pw.value];
    let webSocket = new WebSocket(url);
    webSocket.onmessage = (msg) => {
        // console.dir(JSON.parse(msg.data));
        if (i == msgs.length) {
            webSocket.close();
            let data = JSON.parse(msg.data);
            let worked = data.result !== undefined && data.error === undefined;
            main.className = worked ? 'success' : 'fail';
            main.addEventListener('input', (event) => {main.className = '';}, {once: true});
            i = 0;
            return;
        }
        sendNextMsg(msg.target);
    };
    webSocket.onopen = (event) => {
        // console.dir(event);
        sendNextMsg(event.target);
    };
});

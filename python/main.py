from getpass import getpass
import json
import logging
import websocket

# https://pypi.org/project/websocket-client/
# https://www.truenas.com/docs/api/websocket.html

# What is the id for ?

# logging.basicConfig(level=logging.DEBUG)


def main(ip: str, user: str, pw: str) -> bool:
    uri = f'ws://{ip}/websocket'
    # get connection
    ws = websocket.create_connection(uri)
    # connect
    ws.send(json.dumps({
        'msg': 'connect',
        'version': '1',
        'support': ['1']
    }))
    logging.debug(ws.recv())
    # authenticate
    ws.send(json.dumps({
        'id': '0',
        'msg': 'method',
        'method': 'auth.login',
        'params': [user, pw]
    }))
    logging.debug(ws.recv())
    # shutdown server
    ws.send(json.dumps({
        'id': '0',
        'msg': 'method',
        'method': 'system.shutdown',
        'params': {}
    }))
    ans = ws.recv()
    logging.debug(ans)
    # close connection
    ws.close()
    # return
    ans = json.loads(ans)
    return 'result' in ans and 'error' not in ans


if __name__ == '__main__':
    ip = '192.168.3.99'
    ip = input(f'ip ({ip}) ? ') or ip
    user = 'root'
    user = input(f'user ({user}) ? ') or user
    pw = getpass('password ? ')
    res = main(ip, user, pw)
    print(('fail', 'success')[res])
